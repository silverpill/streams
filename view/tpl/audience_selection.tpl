<div class="generic-content-wrapper">
    <div class="section-title-wrapper clearfix">
        <h2>
        {{$title}}
        </h2>
    </div>
    <div class="descriptive-text">{{$desc}}</div>
    <div class="descriptive-text">{{$desc2}}</div>

    <form action="audience" method="post">
        <div class="form-group field custom">
            <label for="group-selection" id="group-selection-lbl">{{$label}}</label>
            <select class="form-control" name="group_allow" id="group-selection" >
                {{foreach $groups as $group}}
                <option value="{{$group.id}}" {{if $group.selected}}selected="selected"{{/if}} >{{$group.name}}</option>
                {{/foreach}}
            </select>
        </div>
        <div class="settings-submit-wrapper" >
            <button type="submit" name="submit" class="btn btn-primary">{{$submit}}</button>
        </div>
    </form>
</div>
