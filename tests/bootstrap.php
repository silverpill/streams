<?php

set_include_path(
    '../include' . PATH_SEPARATOR
    . '../library' . PATH_SEPARATOR
    . '../'
);

require_once('../boot.php');