An open source fediverse server with a long history of innovation. See [FEATURES](https://codeberg.org/streams/streams/src/branch/dev/FEATURES.md).

The software supports a wide range of online behaviour, from personal communications with closed media access - all the way to fully public broadcasting. The default settings tend to favor personal and private use. Adjust as desired. 

This repository uses a community-driven model. This means that there are no dedicated developers working on new features or bug fixes or translations or documentation. Instead, it relies on the contributed efforts of those that choose to use it.

This software is dedicated to the public domain to the extent permissible by law and is not associated with any consumer brand or product.

A fediverse support group exists at 
https://fediversity.site/channel/streams

