<?php
namespace Code\Module;

use App;
use Code\Lib\Libzot;
use Code\Lib\Channel;
use Code\Web\Controller;

/* fix missing or damaged hublocs */

/*
 * The purpose of this procedure is to repair the site signatures of local channels
 * if that signature changes. This will be an extremely rare event. You will probably
 * be directed to use this tool if your site is reporting an invalid signature in all
 * or nearly of its interactions.
 */


class Fhublocs extends Controller {

	function get() {

		if (!is_site_admin() && !isCli()) {
            return '';
        }

		$output = '';

		$r = q("select * from channel where channel_removed = 0");
		$sitekey = get_config('system','pubkey');

		if ($r) {
            // For each local channel
			foreach ($r as $rr) {

				$found = false;
				$primary_address = '';
                // Find all the location (hubloc) records
				$x = Libzot::getLocations($rr['channel_hash']);


                // See if this channel has a valid hubloc record for this site.
				if ($x) {
					foreach ($x as $xx) {
						if ($xx['hubloc_url'] === z_root() && $xx['hubloc_sitekey'] === $sitekey) {
							$found = true;
							break;
						}
					}
					if ($found) {
						$output .= 'Hubloc exists for ' . $rr['channel_name'] . EOL;
						continue;
					}
				}

                // If we got this far, the channel does not have a valid hubloc
                // record for this site/instance. We will need to create one.

                // Determine if this is the primary location for this channel.
                // Since we don't have a valid hubloc record, check for this situation
                // by seeing if this site address matches the associated xchan record.
                // If the xchan record is also missing (an even more rare event), assume
                // that this is the primary.

				$y = q("select xchan_addr from xchan where xchan_hash = '%s' limit 1",
					dbesc($rr['channel_hash'])
				);
				if ($y) {
                    $primary_address = $y[0]['xchan_addr'];
                }
				$hub_address = Channel::get_webfinger($rr);

				$primary = (($hub_address === $primary_address) ? 1 : 0);
				if (! $y) {
                    $primary = 1;
                }

                // Remove any hublocs associated with this channel and this site.
                // By definition, they are not valid.

				$m = q("delete from hubloc where hubloc_hash = '%s' and hubloc_url = '%s' ",
					dbesc($rr['channel_hash']),
					dbesc(z_root())
				);

				// Create a verified hub location pointing to this site.

				$h = hubloc_store_lowlevel(
					[
						'hubloc_guid'      => $rr['channel_guid'],
						'hubloc_guid_sig'  => $rr['channel_guid_sig'],
						'hubloc_hash'      => $rr['channel_hash'],
						'hubloc_id_url'    => Channel::url($rr),
						'hubloc_addr'      => Channel::get_webfinger($rr),
						'hubloc_primary'   => intval($primary),
						'hubloc_url'       => z_root(),
						'hubloc_url_sig'   => Libzot::sign(z_root(), $rr['channel_prvkey']),
						'hubloc_site_id'   => Libzot::make_xchan_hash(z_root(), $sitekey),
						'hubloc_host'      => App::get_hostname(),
						'hubloc_callback'  => z_root() . '/nomad',
						'hubloc_sitekey'   => $sitekey,
						'hubloc_network'   => 'nomad',
						'hubloc_updated'   => datetime_convert(),
						'hubloc_connected' => datetime_convert()
					]
				);

				if ($h) {
                    $output .= 'local hubloc created for ' . $rr['channel_name'] . EOL;
                }
                else {
                    $output .= 'DB update failed for ' . $rr['channel_name'] . EOL;
                }
			}

			return $output;
		}
	}
}
