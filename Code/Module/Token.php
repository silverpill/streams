<?php

namespace Code\Module;

use App;
use DBA;
use Code\Web\Controller;
use Code\Identity\OAuth2Server;
use Code\Identity\OAuth2Storage;
use OAuth2\Request;
use OAuth2\Response;
use OAuth2\GrantType;

class Token extends Controller
{

    public function init()
    {

        logger('args: ' . print_r($_REQUEST, true));
        $authHeader = (new HTTPHeaders())->getAuthHeader();

        if ($authHeader && str_starts_with($authHeader,'Basic')) {
            $userpass = base64_decode(substr($authHeader, 6));
            if (strlen($userpass)) {
                list($name, $password) = explode(':', $userpass);
                $_SERVER['PHP_AUTH_USER'] = $name;
                $_SERVER['PHP_AUTH_PW'] = $password;
            }
        }

        $storage = new OAuth2Storage(DBA::$dba->db);
        $server = new OAuth2Server($storage);
        $request = Request::createFromGlobals();
        $response = $server->handleTokenRequest($request);
        $response->send();
        killme();
    }
}
