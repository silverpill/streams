<?php

/** @file */

namespace Code\Daemon;

use Code\Lib\Channel;

class Xchan_photo implements DaemonInterface
{

    public function run(int $argc, array $argv): void
    {

        if ($argc != 3) {
            return;
        }

        $url   = hex2bin($argv[1]);
        $xchan = hex2bin($argv[2]);

        // Some photo sources hang after connect and aren't caught by curl timeout


        $xchans = q("select * from xchan where xchan_hash = '%s'",
            dbesc($xchan)
        );

        if ($xchans && (int)$xchans[0]['xchan_censored'] === 2) {
            $result = q("update xchan set xchan_photo_date = '%s', xchan_photo_l = '%s', xchan_photo_m = '%s', xchan_photo_s = '%s', xchan_photo_mimetype = '%s' where xchan_hash = '%s'",
                dbescdate(datetime_convert()),
                dbesc(z_root() . '/' . Channel::get_default_profile_photo()),
                dbesc(z_root() . '/' . Channel::get_default_profile_photo(80)),
                dbesc(z_root() . '/' . Channel::get_default_profile_photo(48)),
                dbesc('image/png'),
                dbesc($xchan)
            );
            return;
        }


        set_time_limit(90);

        $photos = import_remote_xchan_photo($url, $xchan);
        if ($photos) {
            $result = q(
                "update xchan set xchan_photo_date = '%s', xchan_photo_l = '%s', xchan_photo_m = '%s', xchan_photo_s = '%s', xchan_photo_mimetype = '%s' where xchan_hash = '%s'",
                dbescdate(datetime_convert()),
                dbesc($photos[0]),
                dbesc($photos[1]),
                dbesc($photos[2]),
                dbesc($photos[3]),
                dbesc($xchan)
            );
            if (! $result) {
                logger("xchan update failed for $url");
            }
        }
    }
}
