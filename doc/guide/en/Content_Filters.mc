Content Filters
===============


### Overview

The **Content Filter** app (when installed) allows you to filter incoming content from all sources or from specific connections. The filtering may be based on words, tags, regular expressions, or language.

You can set global filters for all incoming content in the Content Filter app.

You can also set individual filters for each of your Connections. Select "Edit" for any connection, then look under Custom Filter.

If you add filters under **Do not import posts with this text**, these are applied first. Any matching content will be removed.

Then, any filters under **Only import posts with this text** are applied. Only matching content will be kept, and everything that doesn't match will be removed.


### Basic Filters

**TEXT**

**Any text that doesn't start with one of the following: lang= lang!= # $ ? /**
Case-insensitive text match
Example: `apple` (matches "apple", "APPLE", "Apples", "pineapple", "applesauce", etc.)
Example: `low sub` (matches "low sub", "low sub-zero", "Yellow Submarine", etc.)
NOTE: Beware of using short character sequences that will match many different words. You may want to use REGULAR EXPRESSIONS (see below).


**LANGUAGE**

**lang=**
Match language (if it can be identified)
Example: `lang=de` (matches German-language content)

**lang!=**
Match anything except this language (if it can be identified)
Example: `lang!=en` (matches non-English content)


**HASHTAG**

**#**
Match hashtag
Example: `#cats`


**CATEGORY**

**$**
Match category. (Categories are "personal hashtags" that only apply to a single channel. Very few Fediverse platforms support categories. You can enable them for your channel by installing the **Categories** app.)
Example: `$Science`


**ITEMS AND FIELDS**

**?**
Discussed below under **Advanced Filters**


**REGULAR EXPRESSIONS (REGEX)**

**/**
Match a "regular expression". See numerous online help sites such as [Regular-Expressions.info](https://www.regular-expressions.info/) and [regexr.com](https://regexr.com/) for regex assistance.
Example: `/gr[ae]y/` (matches "gray" and "grey")
Example: `/\b[Ww]ar\b/` (matches the whole words "war" and "War", but not "wars", "warning", "software", etc.)


### Advanced Filters

**ITEMS**

**?**
You can do a string/numeric/array/boolean match on the database fields of an item (a post, comment, etc). A complete listing is beyond the scope of this document, but see `install/schema_mysql.sql` and search for `CREATE TABLE IF NOT EXISTS [nomd]`[/nomd]item[nomd]`[/nomd]`. Here is a sampling:
* `body` (text of the message)
* `verb` (what this item does)
* `item_thread_top` (first post in a thread, boolean)
* `item_private` (0 = public message, 1 = restricted message, 2 = direct message)
* `ua` (User-Agent string of delivery agent - custom item attribute that is not in the database)
* ...etc...

Available comparison operators are:
* `?foo ~= baz` -- item.foo contains the string 'baz'
* `?foo == baz` -- item.foo is the string 'baz'
* `?foo != baz` -- item.foo is not the string 'baz'
* `?foo // regex` -- item.foo matches the regular expression 'regex' - automatically wrapped in forward slashes unless you wrap it with some other character, for instance '=regex='.
* `?foo >= 3` -- item.foo is greater than or equal to 3
* `?foo > 3` -- item.foo is greater than 3
* `?foo <= 3` -- item.foo is less than or equal to 3
* `?foo < 3` -- item.foo is less than 3
* `?foo {} baz` -- 'baz' is an array element in item.foo
* `?foo {\*} baz` -- 'baz' is an array key in item.foo
* `?foo` -- true condition for item.foo
* `?!foo` -- false condition for item.foo (The values 0, '', an empty array, and an unset value will all evaluate to false)

Example: `?verb == Announce` (matches ActivityPub "boosts")


**FIELDS**

**?+**
Field match: ?+field item.object
*Usage is undocumented at present*
