Partager ou Relayer des Publications/Commentaires
=================================================

Supposons que vous voyez le contenu (publication ou commentaire) de quelqu'un et que vous souhaitiez que vos connexions le voient également. Vous avez deux façons de le faire (si l'auteur le permet, bien sûr).

Regardez dans le menu déroulant ⚙ de l'élément (publication ou commentaire). Vous y trouverez deux options : "Partager" et "Relayer" ("Share this" et "Repeat this" si pas encore traduit).

### Partager

Cette option permet de créer une nouvelle publication, au nom de votre canal. Elle comprend le contenu que vous partagez, ainsi que tout élément que vous souhaitez ajouter.

L'option "Partager" vous est familière si vous avez utilisé la fonction "Partager" de Facebook. D'autres plateformes l'appellent "Quote-Post", "Quote-Tweet", "Retweet avec commentaire", "Reshare", "Reblog" ou "Attach".

Étant donné que vous êtes le propriétaire de la nouvelle publication, si quelqu'un ajoute un commentaire, il n'apparaîtra que sur *votre* publication. Les commentaires ne sont pas renvoyés à l'auteur original du contenu que vous avez partagé.

Vous pouvez définir l'audience de votre nouvelle publication, afin de contrôler qui peut la voir et qui peut la commenter.

**Quand dois-je utiliser "Partager" ?
Dans toutes les situations où vous souhaitez entamer une nouvelle conversation au sujet de la publication d'une autre personne. Par exemple :
* L'article original a déjà fait l'objet de centaines de commentaires et vous souhaitez en parler avec des personnes que vous connaissez.
* Vous souhaitez orienter la discussion dans une autre direction et ne pas faire dévier la discussion liée à la publication d'origine.
* Vous avez des choses personnelles à dire au sujet de la publication d'origine, et vous ne souhaitez en parler qu'à quelques-uns de vos contacts.
* Vous voulez que le message original apparaisse sur la page d'accueil de votre canal.

**Inconvénients de l'option "Partager"**
* Vous parlez du contenu de quelqu'un dans son dos (à moins que vous ne l'incluiez dans l'audience de votre publication). Certaines personnes peuvent trouver cela dérangeant.
* Ne permet pas à votre audience de participer aux sondages et aux enquêtes éventuellement inclus dans la publication d'origine. 

### Relayer

Cette option diffuse auprès de vos relations la publication *originale*. D'autres plateformes appellent cette fonction "Boost", "Repost" ou "Announce". Le message est répété dans sa forme originale et vous ne pouvez pas y ajouter vos propres mots, ni restreindre le nombre de personnes qui peuvent le voir.

Tous les commentaires (faits par vous ou par d'autres) feront partie de la conversation attachée à la publication originale.

**Quand dois-je utiliser "Relayer" ?
Lorsque vous souhaitez faire connaître la publication de quelqu'un d'autre et ses commentaires, sans créer vous-même une nouvelle publication. Par exemple :
* Quelqu'un demande de l'aide et vous voulez faire passer le message à vos propres contacts.
* Vous souhaitez contribuer à la diffusion d'une annonce, d'une nouvelle ou d'une jolie photo.
* Vous souhaitez diffuser un sondage ou une enquête.

**Inconvénients de l'option "Relayer"**
* La publication d'origine n'apparaîtra pas sur la page d'accueil de votre canal.
* Vous ne pouvez ajouter vos propres réflexions qu'en tant que commentaire sur l'article d'origine.
