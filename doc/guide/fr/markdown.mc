Markdown
========

Le logiciel de ce site ne reconnaît un nombre limité de balises markdown afin d'éviter les mauvaises surprises. Celles-ci surviennent généralement lorsque vous incluez du code informatique ou de la ponctuation additionnelle dans vos messages. L'utilisation de la fonction ***Aperçu*** de l'éditeur avant de partager une publication ou un commentaire est encouragée.  

[h3]Gras et italique[/h3]

Les balises de gras et italique ne sont identifiées comme telles qu'en début de ligne ou après une espace.
Si vous souhaitez inclure les caractères spéciaux "\*" ou "\_" sans les interpréter comme du markdown, précédez-les d'une barre oblique inverse '\' ou encadrez le texte souhaité dans des balises [nobb][nomd]*texte à échapper*[/nomd][/nobb].

[table]
[tr][td]Markdown[/td][td]Résultat[/td][/tr]
[tr][td][nomd] *texte en italique* [/nomd][/td][td] *texte en italique* [/td][/tr]
[tr][td][nomd] _texte en italique_ [/nomd][/td][td] _texte en italique_ [/td][/tr]
[tr][td][nomd] **texte en gras** [/nomd][/td][td] **texte en gras** [/td][/tr]
[tr][td][nomd] __texte en gras__ [/nomd][/td][td] __texte en gras__ [/td][/tr]
[tr][td][nomd] ***texte en gras et en italique*** [/nomd][/td][td] ***texte en gras et en italique*** [/td][/tr]
[tr][td][nomd] ___texte en gras et en italique___ [/nomd][/td][td] ___texte en gras et en italique___ [/td][/tr]

[/table]

[h3]Titres[/h3]

Les balises des différents niveaux de titres doivent se trouver en début de ligne et être séparés du texte du titre par au moins un caractère d'espacement. Le rendu exact dépend du thème utilisé, certains titres de niveaux différents peuvent sembler avoir la même taille, ils peuvent inclure des soulignements ou encore d'autres mises en forme de texte.

[table]
[tr][td]Markdown[/td][td]Résultat[/td][/tr]
[tr][td][nomd]Titre de niveau 1<br>=================[/nomd][/td][td]<h1>Titre de premier niveau</h1>[/td][/tr]
[tr][td][nomd]# sous-titre de niveau 2[/nomd][/td][td]
# sous-titre de niveau 2[/td][/tr]
[tr][td][nomd]## sous-titre de niveau 3[/nomd][/td][td]
## sous-titre de niveau 3[/td][/tr]
[tr][td][nomd]### sous-titre de niveau 4[/nomd][/td][td]
### sous-titre de niveau 4[/td][/tr]
[tr][td][nomd]#### level 5 header[/nomd][/td][td]
#### sous-titre de niveau 5[/td][/tr]
[tr][td][nomd]##### sous-titre de niveau 6[/nomd][/td][td]
##### sous-titre de niveau 6[/td][/tr]

[/table]

[h3]Code et Citations[/h3]

La spécification markdown définit les blocs de code comme n'importe quelle ligne commençant par 4 espaces ou par une tabulation. Cette règle syntaxique particulière n'est pas prise en charge par le logiciel de ce site pour éviter que du texte normal ne soit affiché comme un bloc de code . En outre, pour être reconnu et affiché comme tel le code en ligne doit être précédé d'au moins un caractère d'espacement ***ou*** se trouver au début d'une ligne et ne peut pas inclure de sauts de ligne. Si vous souhaitez insérer des caractères d'apostrophe inversée sans déclencher un bloc de code, faites-les précéder d'une barre oblique inverse ou entourez le texte des balises [nobb][nomd][/nomd][/nobb].

[table]
[tr][td]Markdown[/td][td]Résultat[/td][/tr]
[tr][td][nomd]
```<br>
Ceci est un bloc de code<br>
sur plusieurs lignes<br>
```<br>
[/nomd][/td][td]
```
Ceci est un bloc de code
sur plusieurs lignes
```
[/td][/tr]
[tr][td][nomd]
Ceci est un exemple de `code en ligne`.
[/nomd][/td][td]
Ceci est un exemple de `code en ligne`.
[/td][/tr]
[tr][td][nomd]
> Ceci est une citation qui peut<br>
> s'étendre sur plusieurs lignes.<br>
[/nomd][/td][td]
> Ceci est une citation qui peut
> s'étendre sur plusieurs lignes.
[/td][/tr]

[/table]

[h3]Liens et images[/h3]

[table]
[tr][td]Markdown[/td][td]Résultat[/td][/tr]
[tr][td][nomd]
[Ceci est un hyperlien]([baseurl])
[/nomd][/td][td]
[Ceci est un hyperlien]([baseurl])
[/td][/tr]
[tr][td][nomd]
![Ceci est une image]([baseurl]/images/zot-300.png)
[/nomd][/td][td]
![Ceci est une image]([baseurl]/images/zot-300.png)
[/td][/tr]
[/table]


[h3]Listes[/h3]

Afin de réduire les "faux positifs", les listes markdown ne sont rendues que si on trouve plus d'une balise d'élément de liste dans la totalité du la publication.

Comme pour le gras et l'italique, le premier caractère d'une liste non ordonnée peut être précédé d'une barre oblique inverse ou être entouré de balises [nobb][nomd][/nomd][/nobb] afin d'éviter que les lignes de texte normal commençant par ces caractères ne soient interprétées comme une liste.  

[table]
[tr][td]Markdown[/td][td]Résultat[/td][/tr]
[tr][td][nomd]
* élément de liste 1<br>
* élément de liste 2<br>
* élément de liste 3<br>
[/nomd][/td][td]
* élément de liste 1
* élément de liste 2
* élément de liste 3
[/td][/tr]
[tr][td][nomd]
- élément de liste 1<br>
- élément de liste 2<br>
- élément de liste 3<br>
[/nomd][/td][td]
- élément de liste 1
- élément de liste 2
- élément de liste 3
[/td][/tr]
[tr][td][nomd]
+ élément de liste 1<br>
+ élément de liste 2<br>
+ élément de liste 3<br>
[/nomd][/td][td]
+ élément de liste 1
+ élément de liste 2
+ élément de liste 3
[/td][/tr]
[tr][td][nomd]
1. élément de liste 1<br>
2. élément de liste 2<br>
3. élément de liste 3<br>
[/nomd][/td][td]
1. élément de liste 1
2. élément de liste 2
3. élément de liste 3
[/td][/tr]
[tr][td][nomd]
1) élément de liste 1<br>
2) élément de liste 2<br>
3) élément de liste 3<br>
[/nomd][/td][td]
1) élément de liste 1
2) élément de liste 2
3) élément de liste 3
[/td][/tr]
[/table]
