Services de localisation
========================

Les services de localisation sont des outils permettant d'utiliser les informations de géolocalisation dans les messages partagés sur le Fediverse. Il existe un certain nombre d'outils différents.

Un addon *Fournisseur de cartes*, est un outil de localisation important puisqu'il permet d'afficher des cartes intégrées correspondant aux données de géolocalisation. Actuellement, un seul addon Fournisseur de cartes est disponible - "openstreetmap". Un seul addon Fournisseur de cartes à la fois peut être installé/actif sur le système, donc si quelqu'un crée un addon Fournisseur de cartes Google et que vous souhaitez l'utiliser sur votre site, l'addon openstreetmap devra être désactivé.

Les données de géolocalisation peuvent être saisies sous forme textuelle, comme "4 rue Jean Rey, Paris", ou de coordonnées géographiques comme "-21.363,55.756". Il est important de retenir que les localisations textuelles peuvent être ambiguës, par exemple il existe une ville de "Santa Cruz" en Californie, aux États-Unis, mais aussi une en Bolivie. Par conséquent, de nombreux services de géolocalisation de ce logiciel requièrent l'utilisation de coordonnées géographiques, en raison de leur plus grande précision. Il est possible d'afficher des cartes pour les localisations textuelles, mais il est préférable d'utiliser les coordonnées pour obtenir un accès complet aux outils de géolocalisation disponibles. Si des coordonnées ressemblant à celles d'une position sur une carte sont présentes dans le champ de localisation textuelle, une extraction sera tentée. 

## Paramètres de géolocalisation

Les paramètres de géolocalisation se trouvent sur la page principale des paramètres de votre canal. Commencez par sélectionner 'Paramètres' dans le menu des applications (menu "hamburger menu" en haut à droite de la page).

![capture d'écran montrant les paramètres de géolocalisation]([baseurl]/doc/guide/fr/locationsettings.png)

Les paramètres disponibles sont les suivants:

### Géolocalisation par défaut des publications
Saisissez une géolocalisation textuelle si vous le souhaitez. Ce texte sera joint à tous vos messages et commentaires. Certains logiciels du fediverse peuvent tenter d'afficher cette localisation si elle est relativement peu ambiguë. Souvent utilisé pour indiquer la région ou le pays d'où vous postez.

### Obtenir la géolocalisation de votre publication via votre navigateur web ou votre appareil
Si cette option est activée, votre navigateur vous demandera la permission d'obtenir la position de votre appareil chaque fois que vous publiez quelque chose. Vous pouvez également autoriser cette opération à se faire automatiquement. Cette méthode utilise la géolocalisation de votre navigateur, qui est généralement assez précise sur les appareils mobiles et portables, mais qui est souvent très imprécise (dans de nombreux pays) si vous utilisez un ordinateur de bureau.

### Ignorer votre navigateur/appareil et utiliser ces coordonnées (latitude,longitude)
Ce paramètre est souvent utilisé sur les ordinateurs de bureau, afin d'outrepasser la localisation du navigateur si elle est incorrecte. Les données saisies doivent avoir le format "latitude, longitude" et consister en deux nombres à point flottant représentant vos coordonnées géographiques. Si les deux nombres sont égaux à 0, les services de géolocalisation ne seront pas activés (bien qu'il s'agisse d'une localisation valide au large des côtes africaines).


## Utiliser la géolocalisation dans les publications
Les services de localisation disposent de plusieurs boutons dans l'éditeur de messages. Ceux-ci sont figurent sur la capture d'écran suivante:

![capture d'écran montrant les boutons liés à la géolocalisation dans l'éditeur de publication]([baseurl]/doc/guide/fr/locationwitheditor.png)

Certains de ces boutons, voire tous, peuvent être absents et n'pparaître que si une localisation basée sur les coordonnées géographique a déjà été spécifiée.

1. Le premier bouton (représenté par un globe) est toujours affiché. Il vous permet de définir ou de modifier votre position actuelle. Un champ de saisie de texte est prévu. Vous pouvez fournir des coordonnées géographiques au format "latitude, longitude" **ou** vous pouvez saisir un point '.' qui indique à votre navigateur de vous demander pour cette fois seulement d'insérer l'emplacement actuel de votre appareil dans la publication en cours.

2. Le bouton suivant (représenté par un cercle vide) est proposé chaque fois que des données de localisation ont été insérées dans la publication en cours d'édition. En cliquant sur ce bouton, vous supprimez vos données de localisation de la publication.

3. Les deux boutons suivants s'affichent si une position géographique est incluse dans la publication. Il s'agit des icônes "entrer" et "quitter", qui permettent de transformer la publication actuelle en une activité en ligne "arrivée" (checkin) ou "départ" (checkout). Lorsque l'une de ces options est sélectionnée, l'icône change de couleur pour indiquer que le message est désormais une activité d'arrivée/départ et une carte est insérée dans le message. Vous pouvez ajouter du texte supplémentaire ou tout autre contenu. Utilisez le bouton de prévisualisation (icône "œil") pour visualiser les résultats sans les publier ou cliquez sur "Partager" pour publier. Si vous cliquez une deuxième fois sur l'un de ces deux boutons, l'action est annulée et le message redevient un message normal.

![prévisualisation d'un post d'activité de checkin]([baseurl]/doc/guide/fr/checkin.png)

## Recherche à proximité
Toute publication incluant des coordonnées géographiques peut être utilisée pour effectuer une recherche à proximité. Cela vous permet de consulter rapidement (par exemple) les avis sur une zone touristique en effectuant une recherche à proximité sur tout message publié à proximité de cette zone touristique. Pour effectuer une recherche à proximité, cliquez sur l'avatar ou photo de l'auteur du message. Un mini-panneau s'ouvre alors avec un certain nombre d'options. Si le message contient des coordonnées géographiques, l'une de ces options sera "À proximité" (ou "Nearby"), ce qui permettra d'effectuer une recherche à proximité sur la base de ce message.

![capture d'écran montrant comment naviguer vers la recherche à proximité]([baseurl]/doc/guide/fr/nearby.png)
