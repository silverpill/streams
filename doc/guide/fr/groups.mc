Groupes
=======


Les groupes facilitent la communication sur des sujets spécifiques en offrant un espace commun pour interagir.


Créer un Groupe
===============

Ouvrez le menu de votre profil. Lorsque vous utilisez le thème standard/par défaut, vous pouvez le faire en cliquant sur votre photo en haut tout à gauche de la barre de navigation supérieure. Sélectionnez "Canaux". Sur la page de gestion des canaux, cliquez sur "Nouveau". Dans "Rôle et confidentialité du canal", sélectionnez l'une des entrées "Groupee /  Communauté" dans la liste déroulante. Créez un nom de groupe et un nom d'utilisateur court au choix, puis validez. 

**Groupe - Normal**

Cette sélection crée un groupe public dont les membres sont par défaut automatiquement approuvés. Les messages du groupe sont partagés publiquement. Les membres du groupe peuvent téléverser des fichiers/photos.

**Groupe - Limité**

Cette sélection crée un groupe public dont les membres sont par défaut automatiquement approuvés. Les messages du groupe sont partagés publiquement. Les membres du groupe **ne peuvent pas** téléverser des fichiers/photos.

**Groupe - Modéré**

Cette sélection crée un groupe public qui exige que les publication et les commentaires des nouveaux membres soient modérés (approuvés par un administrateur) avant d'être publiés. Cette fonction peut être désactivée à tout moment pour les membres existants du groupe en modifiant la connexion et en désélectionnant l'autorisation "Modéré" pour ce canal. Les membres du groupe **ne peuvent pas** téléverser des fichiers/photos.

**Groupe - Restricted**

Cette sélection crée un groupe privé dont les membres doivent être approuvés. Les messages du groupe ne sont partagés qu'avec les membres du groupe. Les membres du groupe peuvent téléverser des fichiers/photos. 


Une fois le nouveau canal créé, vous pouvez configurer la photo de profil, la photo de couverture et les détails du profil comme vous le souhaitez. Pour revenir à votre canal d'origine, sélectionnez "Canaux" dans le menu du profil et sélectionnez votre canal d'origine dans la liste fournie.Vous pouvez également souhaiter rejoindre (vous connecter avec) le groupe depuis votre canal d'origine, car cela ne se fait pas automatiquement. 


Rejoindre un Groupe
===================

Rejoignez un groupe de la même manière que vous vous connectez à n'importe quel canal du réseau. Si l'option de connexion n'est pas proposée, ouvrez votre application Connexions et saisissez l'adresse web ou l'URL dans la zone de texte prévue à cet effet sous "Ajouter une nouvelle connexion". 
Pour les groupes publics, vous pouvez être automatiquement approuvé, selon ce qu'a défini de l'administrateur du groupe. 


Quitter un Groupe
=================

Pour quitter un groupe, supprimez la connexion à partir de l'application Connexions.



Publier dans un Groupe
======================

Il y a plusieurs façons de publier dans un groupe.

1) Rédigez un message sur votre propre site et utilisez la boîte de gestion des droits d'accès (cliquez sur l'icône cadenas située immédiatement sous l'éditeur de messages) pour sélectionner le groupe comme destinataire. C'est la méthode à privilégier, car elle s'efforce d'optimiser la communication avec les groupes proposés par d'autres applications.

2) Envoyez un message direct (ou message privé, ou "DM") au groupe. Ce message sera republié sous la forme d'un message de groupe. Un message direct est défini comme tel en sélectionnant le groupe comme destinataire dans la boîte de gestion des droits d'accès (icône cadenas sous l'éditeur de messages). Vous pouvez également créer un message en utilisant des "mentions privées" et en étiquetant le groupe.  Un message contenant une mention privée n'est envoyé qu'aux personnes mentionnées. Une mention privée commence par les caractères @! suivis du nom du groupe. Ce texte devrait se compléter automatiquement dès que vous commencez à taper le nom.

Lorsque vous utilisez un message direct pour publier dans un groupe, veillez à ne pas inclure d'autres destinataires.

Les membres d'un groupe hébergé sur votre site qui utilisent d'autres logiciels (Mastodon, MoodleNet, etc.) devront utiliser un message direct pour y publier du contenu. Le mécanisme précis d'envoi d'un message direct peut varier en fonction du logiciel utilisé.

3) Visitez la page d'accueil du groupe à l'aide d'un lien authentifié et publiez un message directement celle-ci, comme vous le feriez sur votre site. Cette option n'est disponible qu'entre les sites et les applications du réseau qui fournissent système d'authentification unifié. Ce logiciel offre un tel service. D'autres applications peuvent ne pas le faire.


Publier un commentaire dans un Groupe
=====================================

Aucune action particulière n'est nécessaire. Répondez à la conversation dans la boîte de commentaires prévue à cet effet, comme vous le feriez pour n'importe quelle autre conversation. 

