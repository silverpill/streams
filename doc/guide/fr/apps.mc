Applications
============

Les applications constituent la principale méthode d'interaction avec ce logiciel. Chaque canal dispose d'une collection d'"Applications Système" qui remplissent des fonctions spécifiques et importantes. D'autres applications sont disponibles sur la page "Application disponibles" et offrent des fonctionnalités et des options supplémentaires. Une fois qu'une application est installée, vous pouvez utiliser ses fonctionnalités. 

Sur la page [Applications Installées]([baseurl]/apps), vous pouvez consulter et gérer votre catalogue d'applications. Des boutons sont disponibles pour ajouter en un clic des applications au menu principal des applications ou dans la barre de navigation.


Available apps
===============

### Audience

Cette application vous permet de modifier votre audience par défaut. Cela affecte les permissions pour les messages et les médias. Cette application vous permet d'outrepasser l'audience par défaut basée sur votre type de canal, et vous donne le choix entre Public, Connexions, ou n'importe laquelle de vos listes d'accès.

### Catégories

Cette application vous permet d'ajouter des catégories nominatives à vos publications. La principale différence entre les catégories et les hashtags est que la recherche par catégorie ne renvoie qu'à vos propres publications personnelles. La recherche par hashtags porte sur toutes les publications de tous les canaux de du site.

### Mon canal

Installée par défaut. Cette application permet d'accéder à la page d'accueil de votre canal.

### Clients

Cette application vous permet de créer et de gérer des identifiants d'accès Oauth2, qui sont généralement utilisés pour fournir une authentification pour les applications mobiles et les applications clientes tierces.

### Comment Control

Cette application vous permet de contrôler la possibilité de commenter vos articles. Vous pouvez choisir de désactiver les commentaires, de désactiver les commentaires après une date/heure spécifique et de sélectionner différents publics autorisés à commenter. Par exemple, "tout le monde" ou "seulement mes amis". Les commentaires entrants seront rejetés par votre site/instance s'ils ne sont pas autorisés. La notification de ces rejets à l'expéditeur dépend des capacités de son propre logiciel.

### Connexions

Installée par défaut. Cette application vous permet de gérer vos connexions (comptes suivis et abonnés) ainsi que leurs autorisations et leurs paramètres de filtrage.

### Filtre de contenu

Permet de spécifier différentes règles pour les publications et commentaires entrants, soit en tant que filtre global s'appliquant à toutes les publications entrantes, soit par connexion. Le filtrage peut s'effectuer à l'aide de hashtags, de catégories, d'expressions régulières, de contenu textuel et de correspondance linguistique. Les publications et les commentaires qui correspondent aux règles sont rejetés et ne sont pas stockés.

### Importer du contenu

Exige que l'administrateur du site installe le module complémentaire "content_import". Cette application permet de migrer le contenu d'une autre instance de ce canal. Son utilisation ne devrait pas être nécessaire, à moins qu'il n'y ait eu des problèmes d'importation de contenu et/ou de stockage de fichiers lorsque le canal a été importé pour la première fois.

### Annuaire

Installé par défaut. Cette application permet de découvrir des comptes au sein du réseau. Il affiche tous les comptes connus de votre site web/instance, à l'exception de ceux qui choisissent de demeurer "cachés".

### Drafts

Cette application, non-installée par défaut, vous permet d'enregistrer une publication ou un commentaire que vous êtes en train de rédiger pour le compléter/modifier plus tard, avant de le publier. Une fois l'installation effectuer, vous pourrez cliquer sur l'icône "disquette" dans l'éditeur de publications/commentaires pour enregistrer un brouillon. Cliquez sur l'application elle-même pour afficher vos brouillons actuels puis cliquez sur "Modifier" dans le menu déroulant de l'item pour poursuivre la rédaction.

Important : utilisez l'icône Brouillon (qui ressemble à une disquette dans le thème par défaut) pour continuer à sauvegarder le brouillon si vous souhaitez continuer à travailler dessus plus tard. Dès que vous aurez cliqué sur "Partager" (articles) ou "Envoyer" (commentaires), il sera publié.

### Évènements

Installée par défaut. Cette application vous permet d'accéder à votre calendrier personnel et à vos événements. Les anniversaires de vos amis sont automatiquement ajoutés à votre calendrier (pour ceux qui ont saisi l'information dans leur profil), ainsi que les événements auxquels vous avez choisi de participer. Si vous souhaitez recevoir un rappel pour d'autres événements, veuillez sélectionner "Ajouter au calendrier" dans le menu du message souhaité.

### Expiration des publications

Cette applicaiton vous permet de définir de façon globale le nombre de jours au-delà duquel chaque publication sera supprimée. Vous pouvez également pour chaque publication définir une date et une heure d'expiration personnalisées.

### Fichiers

Installée par défaut. Cette application permet d'accéder à votre stockage en ligne et à vos ressources WebDAV (tout ce que vous téléversez).

### Followlist

Cette application n'est présente que si l'addon 'followlist' est installé par l'administrateur du site. Elle vous permet de vous connecter en rapidement à une liste de canaux. Typiquement, il s'agit de la collection ActivityPub 'abonnements' (ou 'suivis') provenant dun autre serveur. Les fichiers d'export de contacts Mastodon sont également pris en charge.

### Friend Zoom

Ajoute un curseur en haut de votre flux. Chaque connexion peut se voir attribuer une valeur comprise entre 0 et 100. La valeur par défaut est 80 pour les nouvelles connexions. Dans votre flux les personnes inconnues ont une valeur de 100, et vous de 0. À vous d'ajuster vos connexions entre ces deux valeurs. Vous pouvez utiliser le curseur pour zoomer sur vos amis proches ou dézoomer pour visualiser les personnes/canaux que vous connaissez moins bien ou qui publient très/trop fréquemment au point de noyer les publications de tous les autres.

### Publications programmées

Cette application ajoute une option vous permettant de définir une date/heure de mise en ligne de vos publications (généralement utilisée pour publier automatiquement des messages pendant que vous êtes en vacances). Programmer une publication requiert un délai d'au moins 15 minutes dans le futur, faute de quoi elle sera immédiatement mise en ligne (avec les date et heure définies).

### Fuzzloc

Disponible si l'addon 'fuzzloc' a été installé par l'administrateur du site. Lorsque vous utilisez les services de localisation du navigateur pour marquer l'emplacement de votre message, cela vous permet de définir une distance minimale/maximale par rapport à cet emplacement précis. Une localisation aléatoire sera générée en fonction de ces paramètres.  

### Galerie

Disponible si l'addon "galerie" a été installé par l'administrateur du site. Ce module fournit un navigateur d'images amélioré pour vos photos par rapport à l'application "Photos" standard. Les interactions avec les photos de votre flux sont légèrement modifiées.

### Guest Pass

Cette application vous permet de créer des comptes d'invités et/ou de créer des liens qui peuvent accéder à certaines de vos ressources privées. Par exemple, si vous souhaitez envoyer des liens par courrier électronique pour permettre aux grands-parents de voir une photo privée de leurs petits-enfants.

### Hexit

Disponible si l'addon 'hexit' a été installé par l'administrateur du site. Il s'agit d'un convertisseur hexadécimal/décimal en ligne.

### Langue

Cette application est généralement utilisée par les développeurs pour tester d'autres langues et est rarement nécessaire. La langue de votre site web dépend des paramètres linguistiques de votre navigateur et de l'existence de traductions disponibles. Cette application vous permet de passer rapidement outre les paramètres par défaut et de spécifier une langue particulière.

### Lists

Installé par défaut. Certains logiciels appellent cette fonctionnalité 'Aspects' (Diaspora) ou 'Cercles' (Google+). Cela vous permet de créer/gérer des listes de vos connexions, pour visualiser les publications de leurs membres, ou pour partager du contenu avec l'ensemble des membres d'une liste.

### Balisage

Fournit des boutons de mise en forme pour les caractères gras, italiques, soulignés, etc. Sinon, les codes correspondants doivent être saisis manuellement. Ce logiciel reconnaît Markdown, HTML et bbcode.

### Notes

Fournit un simple post-it sur votre page "Flux" pour conserver de courtes notes et ou faire office d'aide-mémoire.

### NSFW

"Not Safe For Work". Available if the 'nsfw' addon has been installed on this site. By default this will collapse and hide posts matching any of your rules unless you are running in "safe mode". These rules are based on hashtags, text content, regular expressions, and language.

It is like the 'Content Filter' app except that all matching posts are present in your stream but require you to open each such message to view. Posts matching 'Content Filter' rules are rejected and will not be present in your stream.

You can select whether or not you are running in "safe mode" in your personal menu, and this setting is preserved separately for each login. This means your work computer can be in 'safe mode' and questionable content hidden, while your home computer may be configured to show everything by default.

"Not Safe For Work".  Disponible si l'addon 'nsfw' a été activé par l'administrateur. Par défaut, cette option réduit et masque les messages qui correspondent à l'une de vos règles, à moins que vous ne soyez en "mode sécurisé". Ces règles s'appuient sur les hashtags, le contenu du texte, des expressions régulières et la langue.

Il s'agit d'une application similaire à l'application 'Filtre de contenu', à la différence qu'ici tous les messages correspondants restent présents dans votre flux, vous devez simplement ouvrir chacun d'entre eux pour les consulter. Les messages correspondant aux règles du 'Filtre de contenu' sont eux rejetés et n'apparaissent pas du tout dans votre flux.

Vous pouvez choisir de fonctionner ou non en "mode sûr" dans votre menu personnel, et ce paramètre est conservé séparément pour chaque connexion. Cela signifie que votre ordinateur professionnel peut être en "mode sûr" et que les contenus douteux peuvent être masqués, tandis que votre ordinateur personnel peut être configuré pour tout afficher par défaut.

### Géolocalisation photo

Si vos photos téléversées sont géolocalisées, cet application permet d'afficher une carte de ces lieux dans l'application 'Photos'. Pour ce faire, l'administrateur du site peut être amené à installer un addon fournissant des cartes, tel que 'openstreetmap'.

### Photos

Installée par défaut. Cette application permet d'afficher les photos et les albums photo téléversés, indépendamment des autres fichiers téléchargés.

### Qrator

Disponible si l'addon 'qrator' a été installé par l'administrateur. Il s'agit d'une page permettant de générer des QR codes en fonction de vos besoins.

### Étiquetage arc-en-ciel

Disponible si l'addon 'rainbowtag' a été installé par l'administrateur. Nécessite également l'application Tagadelic. Ceci affiche le nuage de tags fournis par Tagadelic en couleurs au lieu de monochrome.

### Roles

Cette application vous permet de créer des paramètres de permissions spécifiques pour différentes classes/types de connexions, que vous nommerez comme vous le souhaitez. Vous pouvez par exemple les appeler "amis", "abonnés" ou "modérateurs", en choisissant précisément les autorisations à associer à ces noms.
     
### Recherche

Accès à la page de recherche.

### Secrets

Cette application fournit un cryptage de bout en bout basique en utilisant la Stanford Javascript Crypto Library et vous permet de définir une clé de déchiffrement (mot de passe ou phrase secrète) et de façon optionnelle un indice. Un bouton de cryptage est ajouté à l'éditeur de messages. La clé de déchiffrement doit être communiquée à l'aide d'un autre service ou reposer sur la base de connaissances ou d'une expérience communes. Le bouton de cryptage cryptera l'ensemble du contenu de la fenêtre de message, qui peut inclure des balises bbcode de base telles que le texte en gras et en italique. Pour de meilleurs résultats, veuillez limiter votre contenu à du texte brut, car les balises non prises en charge seront affichées mot pour mot. Les liens sont autorisés, mais devraient probablement être évités, car leur récupération à l'autre bout pourrait révéler des informations secrètes. 

### Administrateur

Si vous êtes l'administrat·eur·rice du site, cette application vous permet d'accéder à l'interface d'administration.

### Sites

Fournit une liste catégorisée de tous les sites et instances du Fediverse dont ce site a connaissance.

### Flux

Installée par défaut. Affiche la page sur laquelle apparaissent toutes vos les conversations auxquelles vous et vos connexions participez.

### Ordre du flux

Disponible si l'addon 'stream_order' est installé sur le site. Affiche un sélecteur sur la page de Flux permettant de choisir entre différents ordres de tri, tels que "Derniers commentaires", "Dernières publications", "Derniers éléments reçus" et "En vrac par date". Pour les 3 premiers, les activités sont groupées par conversation, pour le dernier chaque activité est affichée en tant que message séparé.

### Suggérer des canaux

Installée par défaut. Cette application parcourt les connexions de vos propres connexions si vous avez l'autorisation de les voir. Elle génère une page de canaux avec lesquels vous n'êtes pas encore connecté, classés en fonction du nombre de vos connexions connectés avec les canaux en question.

### Tagadelic

Application nommée d'après le module Drupal du même nom. Elle fournit un nuage de tags de vos hashtags sur la page d'accueil de votre canal. L'addon "rainbowtag" permet de lui donner un peu plus de couleur.

### Tâches

Fournit une liste de tâches minimaliste sur votre page de Flux (ainsi que sur la page de l'application Tâches).

### Zotpost

Disponible si l'addon 'zotpost' a été installé sur le site. Ceci configure l'addon zotpost pour qu'il transfère automatiquement les publications de votre canal sur un autre site utilisant les protocoles Zot6 ou Nomad.




