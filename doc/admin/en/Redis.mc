# Streams Redis configuration
Redis can be used for faster session data storage and retrieval. Here is how to set it up. 

** Prerequisites:** 
* Redis must be running and available by IP address. You need to know your Redis authentication string before continuing. 
* Be able to edit the Streams .htconfig.php file 
## Step 1: Edit the .htconfig.php file
Streams is configured using a _.htconfig.php_ file in the root folder of the installation. Navigate to your Streams installation folder and open the .htconfig.php file with your preferred editor. 

Append the following code to the bottom of the file.

<pre>// Session storage over Redis
App::$config['system']['session_custom'] = true;
App::$config['system']['session_save_handler'] = "redis";
App::$config['system']['session_save_path'] = "tcp://IP ADDRESS:6379?auth=LONG PASSWORD";</pre> Update the default values to reflect the correct Redis information.

**IP ADDRESS** = The IP address Redis is listening on.
**LONG PASSWORD** = The authentication string used to access Redis.

## Step 2: Verify that Redis is receiving the sessions.
This assumes that Redis is listening on IP 127.0.0.1. Update to reflect the correct IP of your Redis service. 
```
redis-cli -h IP ADDRESS
```
The command will look something like this, and your command prompt will change.
<kbd>redis-cli -h 127.0.0.1
127.0.0.1:6379> </kbd> 
In order to access the session keys, Redis will require password authorization to be submitted beforehand. 
Replace "LONG PASSWORD" in the command below with the Redis password.
```
auth LONG PASSWORD
```
It will look something like this, with a small "OK" message at the end to confirm.
<kbd>127.0.0.1:6379> auth dkdiidlsdifnlsvniefwnciwncl3j92h3vchlcnal3ijcli439d3gflvn:
ok</kbd>
Now, you can view the session keys with this command:
```
keys *
```
It will show you all the session identifiers currently stored in Redis:
<kbd>127.0.0.1:6379> keys *
  1) "PHPREDIS_SESSION:e9310b5ee224b70f8df649f6938c15d9"
  2) "PHPREDIS_SESSION:d2abb4549333b81c9a119dbb639c0357"
  3) "PHPREDIS_SESSION:8cc64f59b80fc95e4ed43078048dcabb"
127.0.0.1:6379> </kbd>
