# Streams SMTP configuration 

Enable Streams to use SMTP by installing and configuring the PHPMAILER plugin.

**Prerequisites:**
* Login to your Streams site as an administrator
* Have command line access to the Streams installation. 
* Be able to edit the Streams .htconfig.php file
## Step 1: Update site email settings
Go to the administrative settings in your Streams instance and navigate to site administration.

You will need to set the 'Reply-to email address for system generated email' and the 'Sender (from) email address for system generated email' to the same email address you intend to use for SMTP email sending. 
Note, in many cases this must be the same as the authenticated user email configured below. 

![Streams site settings](/doc/admin/en/images/site-settings.webp "Streams site settings")

## Step 2: Enable PHPMAILER
Navigate to the Streams root folder on the command line. Then enable the PHPMAILER plugin using this command:
```
./util/addons install phpmailer
```
## Step 3: Configure PHPMAILER
The configuration depends on the SMTP service you are using. Here is a basic example: 

<pre>// PHPMailer addon
App::$config['phpmailer']['mailer'] = 'smtp';
App::$config['phpmailer']['host'] = 'smtp.example.com';
App::$config['phpmailer']['port'] = '587';
App::$config['phpmailer']['smtpauth'] = 1;
App::$config['phpmailer']['smtpsecure'] = 1;
App::$config['phpmailer']['username'] = 'example@example.com';
App::$config['phpmailer']['password'] = 'email password';</pre> You need to edit the example with the information from your SMTP provider. Once you have the correct information, append the entire block of code to the bottom of the .htconfig.php file. 

**Here are examples for some common hosting platforms.**
 
<pre>// K&T Host - www.knthost.com
App::$config['phpmailer']['mailer'] = 'smtp';
App::$config['phpmailer']['host'] = 'smtp.knthost.com';
App::$config['phpmailer']['port'] = '587';
App::$config['phpmailer']['smtpauth'] = 1;
App::$config['phpmailer']['smtpsecure'] = 1;
App::$config['phpmailer']['username'] = 'Full email address';
App::$config['phpmailer']['password'] = 'Email password';</pre>
<pre>// Dreamhost - www.dreamhost.com
App::$config['phpmailer']['mailer'] = 'smtp';
App::$config['phpmailer']['host'] = 'smtp.dreamhost.com';
App::$config['phpmailer']['port'] = '587';
App::$config['phpmailer']['smtpauth'] = 1;
App::$config['phpmailer']['smtpsecure'] = 'tls';
App::$config['phpmailer']['username'] = 'Full email address';
App::$config['phpmailer']['password'] = 'Email password';</pre>  
<pre>// Namecheap - www.namecheap.com
App::$config['phpmailer']['mailer'] = 'smtp';
App::$config['phpmailer']['host'] = 'mail.privateemail.com';
App::$config['phpmailer']['port'] = '587';
App::$config['phpmailer']['smtpauth'] = 1;
App::$config['phpmailer']['smtpsecure'] = 1;
App::$config['phpmailer']['username'] = 'Full email address';
App::$config['phpmailer']['password'] = 'Email password';</pre>
<pre>// Netcup - www.netcup.com
//Note: Host changes depending on server. Check with netcup.net support. 
App::$config['phpmailer']['mailer'] = 'smtp';
App::$config['phpmailer']['host'] = 'CHANGEME.netcup.net';
App::$config['phpmailer']['port'] = '587';
App::$config['phpmailer']['smtpauth'] = 1;
App::$config['phpmailer']['smtpsecure'] = 1;
App::$config['phpmailer']['username'] = 'Full email address';
App::$config['phpmailer']['password'] = 'Email password';</pre>  
